import React, { memo, forwardRef } from 'react';
import Icon from './Icon';

function createIcon(pathes) {
  if(!pathes[0]){
    pathes = [pathes]
  }
  return memo(
    forwardRef((props, ref) => (
      <Icon ref={ref} {...props}>
        {pathes.map((path,index)=>{
          return React.cloneElement(path, {
            key: index
          });
        })}
      </Icon>
    )),
  );
}

export default createIcon;
