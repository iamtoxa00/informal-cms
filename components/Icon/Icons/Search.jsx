import React from "react";
import createIcon from "../createIcon";

export default createIcon([
  <path d="M16.325 14.899l5.38 5.38a1.009 1.009 0 11-1.427 1.426l-5.38-5.38a8 8 0 111.426-1.426h.001zM10 16a6 6 0 100-12 6 6 0 000 12z" />,
]);
