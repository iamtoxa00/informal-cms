import React from 'react';
import createIcon from '../createIcon';

export default createIcon(
  <path d="M18.849 18.848a1.2 1.2 0 01-1.697 0l-6-6a1.2 1.2 0 010-1.696l6-6a1.2 1.2 0 111.697 1.696L13.697 12l5.152 5.152a1.2 1.2 0 010 1.696zm-7.2 0a1.2 1.2 0 01-1.697 0l-6-6a1.2 1.2 0 010-1.696l6-6a1.2 1.2 0 011.697 1.696L6.497 12l5.152 5.152a1.2 1.2 0 010 1.696z" />,
);
