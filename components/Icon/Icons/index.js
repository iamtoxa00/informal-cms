import Home from './Home';
import Pages from './Pages';
import ChevronRightIcon from './ChevronRightIcon';
import ChevronLeftIcon from './ChevronLeftIcon';
import Cog from './Cog';
import ChevronDown from './ChevronDown';

import Edit from './Edit';
import External from './External';
import Link from './Link';

export default {
  Home,
  Pages,
  ChevronRightIcon,
  ChevronLeftIcon,
  ChevronDown,
  Cog,
  Edit,
  External,
  Link
}