import React from 'react';
import createIcon from '../createIcon';

export default createIcon(
  <path d="M12 18a1.2 1.2 0 002.049.848l6-6a1.2 1.2 0 000-1.696l-6-6a1.2 1.2 0 10-1.697 1.696L17.503 12l-5.151 5.152A1.2 1.2 0 0012 18zm-7.2 0a1.2 1.2 0 002.048.848l6-6a1.2 1.2 0 000-1.696l-6-6a1.2 1.2 0 00-1.696 1.696L10.303 12l-5.151 5.152A1.2 1.2 0 004.8 18z" />,
);
