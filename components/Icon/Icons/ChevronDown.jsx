import React from 'react';
import createIcon from '../createIcon';

export default createIcon(
  <path d="M2.469 6.969a.75.75 0 011.062 0L12 15.439l8.469-8.47a.75.75 0 111.062 1.062l-9 9a.751.751 0 01-1.062 0l-9-9a.75.75 0 010-1.062z" />,
);
