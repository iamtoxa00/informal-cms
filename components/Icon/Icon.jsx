import React, { useContext, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './Icon.module.scss';

import variables from './variables.module.scss';


const defaultViewBox = '0 0 24 24';

const getSize = (viewBox) => {
  const [, , width, height] = viewBox.split(' ');
  return {
    width,
    height,
  };
};

const getColor = (color) => {
  if(color[0] == '#'){
    return color;
  } else if(color.indexOf('var') == 0){
    return color;
  } {
    return variables[`color-${color}`];
  }
};

function setColors(color, ref){
  if(ref.current){
    if(typeof color == 'object' && color.default){
      ref.current.style.setProperty('--icon-color', getColor(color.default))
      ref.current.style.setProperty('--icon-color--hover',getColor(color.hover || color.default))
      ref.current.style.setProperty('--icon-color--focus',getColor(color.active || color.default))
    } else if (color != 'var') {
      ref.current.style.setProperty('--icon-color', getColor(color))
      ref.current.style.setProperty('--icon-color--hover',getColor(color))
      ref.current.style.setProperty('--icon-color--focus',getColor(color))
    } else {
      ref.current.style.setProperty('--icon-color--hover', 'var(--icon-color)')
      ref.current.style.setProperty('--icon-color--focus', 'var(--icon-color)')
    }
  }
}

const Icon = React.forwardRef((props, ref) => {
  const iconRef = useRef();
  const {
    className,
    size = 'small',
    color = 'var',
    viewBox = defaultViewBox,
    ...rest
  } = props;

  const classes = cn(className, styles.icon, styles.color_custom, {
    [styles[`size-${size}`]]: viewBox === defaultViewBox
  });

  useEffect(()=>{
    setColors(color, iconRef);
  }, [ref])

  return (
    <svg
      ref={iconRef}
      className={classes}
      focusable="false"
      viewBox={viewBox}
      {...getSize(viewBox)}
      {...rest}
    />
  );
});


const sizes = ['small', 'normal', 'large'];

Icon.propTypes = {
  htmlColor: PropTypes.string,
  size: PropTypes.oneOf(sizes),
  className: PropTypes.string,
  children: PropTypes.node,
};

export default Icon;
