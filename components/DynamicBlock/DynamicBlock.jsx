import Blocks from "components/AdminBlocks";

export default function DynamicBlock(props) {
  const { componentName, type, columns, ...data } = props;

  const Block = Blocks[componentName];

  return (
    <div
      style={{
        gridColumn: `span ${columns || 1}`
      }}
    >
      <Block {...data} />
    </div>
  );
}
