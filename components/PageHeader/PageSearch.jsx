import { useEffect, useRef, useState } from "react";
import SearchIcon from "components/Icon/Icons/Search";
import styles from "./PageSearch.module.scss";

export default function PageSearch(props) {
  const [searchText, setSearchText] = useState("");
  const [focused, setFocused] = useState(false);

  const inputRef = useRef();

  const handleInput = (e)=>{
    setSearchText(e.target.value)
  }

  const handleOpen = ()=>{
    setFocused(true)
  }

  const handleBlur = (e)=>{
    setFocused(false)
  }

  useEffect(()=>{
    if(focused == true){
      if(inputRef.current){
        inputRef.current.focus()
      }
    }
  }, [focused])

  return (
    <>
      <div className={`${styles.pageSearch} ${focused ? styles.active : ""}`}>
        {focused && (
          <>
            <input ref={inputRef} type="text" placeholder="Поиск..." onBlur={handleBlur} onChange={handleInput} value={searchText}/>
          </>
        )}
        <div
          className={styles.search}
          onMouseDown={handleOpen}
        >
          <SearchIcon
            className={styles.searchImg}
            color="var(--searchImg-color)"
          />
        </div>
      </div>
    </>
  );
}
