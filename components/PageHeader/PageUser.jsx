import styles from "./PageUser.module.scss";

import { useState } from "react";
import ChevronDown from 'components/Icon/Icons/ChevronDown'

export default function PageUser(props) {
    const [opened, setOpened] = useState(false);

    const toggleOpened = ()=>{
        setOpened(!opened);
    }

    return (
      <div className={`${styles.pageUser} ${opened?styles.opened:""}`}>
          <div className={styles.wrapper} onClick={toggleOpened}>
            <span className={styles.name}>Вася Пупкин</span>
            <div className={styles.image}>
                <img src="/images/mockUser.jpg" alt=""/>
            </div>
            <div className={styles.down}>
                <ChevronDown className={styles.downImg} color="var(--downImg-color)"/>
            </div>
          </div>

          <div className={styles.list}>
              <div className={styles.item}>Настройки профиля</div>
              <div className={styles.item}>Главная страница</div>
              <div className={styles.item}>Панель управления</div>
              <div className={`${styles.item} ${styles.item__btn}`}>Выйти</div>
          </div>
      </div>
    );
  }
  