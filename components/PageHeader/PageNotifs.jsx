import styles from "./PageNotifs.module.scss";
import NotificationIcon from 'components/Icon/Icons/Notification'

export default function PageNotifs(props) {
  return (
    <div className={styles.pageNotifs}>
      <div className={`${styles.notifs} ${styles.alert}`}>
        <NotificationIcon className={styles.notifsImg} color="var(--icon-color)"/>
      </div>
    </div>
  );
}
