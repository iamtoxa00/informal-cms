import styles from "./PageHeader.module.scss";

import PageSearch from "./PageSearch";
import PageUser from "./PageUser";
import PageNotifs from "./PageNotifs";

export default function PageHeader(props) {
  const { pageTitle = "Название страницы", user } = props;

  return (
    <div className={styles.infoLine}>
      <h2 className={styles.pageTitle}>{pageTitle}</h2>
      <div className={styles.groups}>
        <div className={styles.group}>
          <PageSearch />
          <PageNotifs />
        </div>
        <div className={styles.group}>
          <PageUser />
        </div>
      </div>
    </div>
  );
}
