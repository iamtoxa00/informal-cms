import PropTypes from "prop-types";
import { isClient } from "utils/isServer";
import Link from "next/link";

import styles from "./NavBar.module.scss";

import mockData from "mocks/components/NavBar";
import { useEffect, useState } from "react";
import React from "react";

import Icons from "components/Icon/Icons";
import HomeIcon from "components/Icon/Icons/Home";
import ChevronLeftIcon from "components/Icon/Icons/ChevronLeftIcon";
import ChevronRightIcon from "components/Icon/Icons/ChevronRightIcon";

export default function NavBar({ itemsGroups, width, width_opened }) {
  const [opened, setOpened] = useState(false);

  const toggleNav = () => {
    setOpened(!opened);
  };

  if (isClient()) {
    useEffect(() => {
      document.body.style.setProperty("--navbar_width", width + "px");
      document.body.style.setProperty(
        "--navbar--open_width",
        (width_opened || 300) + "px"
      );
    }, [width]);
  }

  return (
    <nav className={`${styles.navbar} ${opened ? styles.opened : ''}`}>
      <div>
        {itemsGroups &&
          itemsGroups.map((group, groupIndex) => {
            return (
              <div key={groupIndex} className={styles.navbar__group}>
                {group.items.map((item, itemIndex) => {
                  return (
                    <Link key={itemIndex} href={item.url}>
                      <div className={styles.navbar__item} title={item.label}>
                        {item.icon ? 
                          (()=>{
                            const Icon = Icons[item.icon];
                            return <Icon className={styles.navbar__item_icon}/>
                          })()
                        : <HomeIcon className={styles.navbar__item_icon} />}
                        <span className={styles.navbar__item_label}>
                          {item.label}
                        </span>
                      </div>
                    </Link>
                  );
                })}
              </div>
            );
          })}
      </div>
      <div className={styles.navbar__group}>
        <div className={`${styles.navbar__item} ${styles.navbar__item_collapse}`} onClick={toggleNav}>
          {opened ? (
            <ChevronLeftIcon className={styles.navbar__item_icon} />
          ) : (
            <ChevronRightIcon className={styles.navbar__item_icon} />
          )}
        </div>
      </div>
    </nav>
  );
}

NavBar.propTypes = {
  itemsGroups: PropTypes.array,
  width: PropTypes.number,
};

NavBar.defaultProps = {
  ...mockData,
};
