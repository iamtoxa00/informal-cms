import { useEffect, useLayoutEffect, useState } from "react";
import styles from "./SitePageSetter.module.scss";
import mockData from "mocks/components/SitePageSetter.js";

import AnimateHeight from "react-animate-height";
import { Formik } from "formik";
import Form from "components/AdminBlocks/Form/Form";

function PageSection(props) {
  const [edited, setEdited] = useState(false);

  const toggle = () => {
    props.onOpen();
  };

  const submitHandle = (values, { setSubmitting }) => {
    console.log(`send data to: ${props.page_slug}`);
    setSubmitting(false);
    setEdited(false);
  };

  const validateHandle = (values) => {
    const errors = {};
    return errors;
  };

  const onChange = () => {
    setEdited(true);
  };

  return (
    <div className={`${styles.section} ${props.opened ? styles.opened : ""}`}>
      <div className={`${styles.section_header} ${edited ? styles.edited : null}`} onClick={toggle}>
        <span>{props.name}</span>
        {/* {edited && <span>edited</span>} */}
        <div
          className={`${styles.toggle_btn} ${
            props.opened ? styles.active : ""
          }`}
        ></div>
      </div>
      <AnimateHeight duration={500} height={props.opened ? "auto" : "0%"}>
        <div
          className={styles.section_body}
        >
          {props.data && (
            <Form
              content={props.data}
              onSubmit={submitHandle}
              onChange={onChange}
            />
          )}
        </div>
      </AnimateHeight>
    </div>
  );
}

export default function SitePageSetter(props) {
  const [pageData, SetData] = useState(null);
  const [openedSection, SetOpened] = useState(null);

  useEffect(() => {
    SetData(mockData);
  }, [props.page_slug]);

  const openHandle = (index) => {
    if(openedSection == index){
      SetOpened(null);
    } else {
      SetOpened(index);
    }
  };

  return (
    <div className={styles.Setter}>
      {pageData?.sections.map((section, section_index) => {
        return (
          <PageSection
            key={section_index}
            {...section}
            page_slug={props.page_slug}
            opened={section_index == openedSection}
            onOpen={() => {
              openHandle(section_index);
            }}
          />
        );
      })}
    </div>
  );
}
