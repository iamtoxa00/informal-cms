import { useState } from "react";
import styles from "./Form.module.scss";
import FormItem from "./FormItem";
import { Formik } from "formik";

const groupInitValues = (content) => {
  return content
    ?.map((el, index) => {
      if (el.type == "group") {
        return groupInitValues(el.content).flat();
      }

      if (el.type == "input") {
        return [el.name || index, el.value || ""];
      }
    })
    .filter((el) => el);
};

export default function Form(props) {
  const { content, ...otherProps } = props;

  const validateHandle = (values) => {
    if(props.onChange){
      props.onChange()
    }
    
    const errors = {};
    return errors;
  };

  return (
    <div className={styles.form}>
      <Formik
        initialValues={Object.fromEntries(
          props.content?.map((el, index) => {
            if (el.type == "group") {
              return groupInitValues(el.content).flat();
            }

            if (el.type == "input") {
              return [el.name || index, el.value || ""];
            }
          })
        )}
        validate={validateHandle}
        onSubmit={props.onSubmit}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit}>
            {props.content?.map((field, index) => {
              return (
                <FormItem
                  key={index}
                  {...field}
                  value={values[field.name]}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  onSubmit={handleSubmit}
                  onReset={handleReset}
                  formValues={values}
                  disabled={isSubmitting}
                />
              );
            })}
          </form>
        )}
      </Formik>
    </div>
  );
}
