import styles from "./FormItem.module.scss";

export default function FormItem(props) {
  if (props.type == "input") {
    return (
      <label
        className={`${styles.input_wrapper} ${props.disabled?styles.disabled:""}`}
        style={{ gridColumn: `span ${props.columns || 12}` }}
      >
        <input
          className={styles.input_field}
          type={props.input_type || "text"}
          value={props.value}
          onChange={props.onChange}
          name={props.name}
          disabled={props.disabled}
        />
        <span className={styles.input_label}>{props.label}</span>
      </label>
    );
  }

  if (props.type == "submit") {
    return (
      <button
        onClick={props.onClick}
        className={`${styles.button} ${styles.submit_button}`}
        style={{ gridColumn: `span ${props.columns || 12}` }}
        disabled={props.disabled}
        type="submit"
      >
        <span>{props.label}</span>
      </button>
    );
  }

  if (props.type == "reset") {
    return (
      <button
        onClick={props.onClick}
        className={`${styles.button} ${styles.reset_button}`}
        style={{ gridColumn: `span ${props.columns || 12}` }}
        disabled={props.disabled}
      >
        <span>{props.label}</span>
      </button>
    );
  }



  if (props.type == "group") {
    return (
      <div className={styles.form_group}>
        {props.content?.map((groupItem, index) => {
          if (groupItem.type == "input") {
            return (
              <FormItem
                key={index}
                {...groupItem}
                value={props.formValues && props.formValues[groupItem.name]}
                onChange={props.onChange}
                disabled={props.disabled}
              />
            );
          }

          if (groupItem.type == "submit") {
            return (
              <FormItem key={index} {...groupItem} onClick={props.onSubmit} disabled={props.disabled} />
            );
          }

          if (groupItem.type == "reset") {
            return (
              <FormItem key={index} {...groupItem} onClick={props.onReset} disabled={props.disabled} />
            );
          }
        })}
      </div>
    );
  }
}