import CardBlock from './CardBlock/CardBlock';
import List from './List/List';
import Form from './Form/Form';

export default {
  CardBlock,
  List,
  Form
}