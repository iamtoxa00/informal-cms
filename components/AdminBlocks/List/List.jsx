import styles from './List.module.scss';

import ListItem from './ListItem/ListItem'

export default function List(props) {
  const {content, theme, ...otherProps} = props;

  return <div className={styles.list}>
    {content.map((listItem, index)=>{
      if(listItem.type == 'item'){
        return <ListItem key={index} {...listItem}/>
      }
    })}
  </div>;
}
