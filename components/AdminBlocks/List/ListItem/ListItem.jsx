import Link from "next/link";
import styles from "./ListItem.module.scss";

import Icons from "components/Icon/Icons";

export default function ListItem(props) {
  const { ...listItem } = props;

  return (
    <div className={styles.listItem}>
      {listItem.content.map((element, index) => {
        if (element.type == "title") {
          const Tag = element.as || "span";
          return (
            <Tag
              key={index}
              className={`${styles.title} ${element.styledAs ? element.styledAs : ""}`}
            >
              {element.content}
            </Tag>
          );
        }

        if (element.type == "subtitle") {
          const Tag = element.as || "span";
          return (
            <Tag
              key={index}
              className={`${styles.subtitle} ${element.styledAs ? element.styledAs : ""}`}
            >
              {element.content}
            </Tag>
          );
        }

        if (element.type == "links") {
          const Tag = element.as || "span";
          return (
            <div key={index} className={styles.links}>
              {element.content.map((link, linkindex) => {
                if (link.url) {
                  return (
                    <Link key={linkindex} href={link.url}>
                      <a className={`${styles.link} p1`}>
                        <span>{link.label}</span>
                        {link.icon &&
                          (() => {
                            const Icon = Icons[link.icon];
                            return <Icon className={styles.link_icon} />;
                          })()}
                      </a>
                    </Link>
                  );
                }

                if (link.action) {
                  return (
                    <button
                      key={linkindex}
                      onClick={eval(link.action).bind(listItem)}
                      className={`${styles.link} p1`}
                    >
                      <span>{link.label}</span>
                      {link.icon &&
                        (() => {
                          const Icon = Icons[link.icon];
                          return <Icon className={styles.link_icon} />;
                        })()}
                    </button>
                  );
                }
              })}
            </div>
          );
        }
      })}
    </div>
  );
}
