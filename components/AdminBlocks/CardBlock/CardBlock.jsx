import styles from './CardBlock.module.scss';

import CardText from './CardText/CardText'
import CardLabel from './CardLabel/CardLabel'
import CardChart from './CardChart/CardChart'
import Form from '../Form/Form'
import SitePageSetter from '../SitePageSetter/SitePageSetter';

export default function CardBlock(props) {
  const {content, theme, ...otherProps} = props;

  var theme_styles = {};

  if(theme == 'Warning'){
    theme_styles = {
      background: "#ffd659"
    }
  }

  return <div className={styles.card} style={theme_styles}>
    {content.map((textBlock, index)=>{
      if(textBlock.type == 'card_label'){
        return <CardLabel {...textBlock} key={index} className={styles.card_label}/>
      }

      if(textBlock.type == 'text'){
        return <CardText {...textBlock} key={index} className={styles.text_block}/>
      }

      if(textBlock.type == 'chart'){
        return <CardChart {...textBlock} key={index} className={styles.card_chart}/>
      }

      if(textBlock.type == 'form'){
        const submitHandle = (values, { setSubmitting })=>{
          console.log(`send data to: ${textBlock.action_url}`)
          setSubmitting(false);
        };
        return <Form {...textBlock} key={index} className={styles.card_form} onSubmit={submitHandle} />
      }

      if(textBlock.type == 'card_pageSetter'){
        return <SitePageSetter {...textBlock} key={index} className={styles.card_form}/>
      }
    })}
  </div>;
}
