import Link from "next/link";
import styles from "./CardLabel.module.scss";

export default function CardLabel(props) {
  const { content, ...otherProps } = props;

  return (
    <div className={styles.card_label}>
      {content &&
        content.map((block, index) => {
          if (block.type == "text") {
            return (
              <h2 key={index} className={styles.card_label__text}>
                {block.content}
              </h2>
            );
          }

          if (block.type == "links") {
            return (
              <div key={index} className={styles.card_label__links}>
                {block.content &&
                  block.content.map((link, linkIndex) => {
                    return (
                      <Link key={linkIndex} href={link.url}>
                        <button
                          className={styles.card_label__link}
                        >
                          {link.label}
                        </button>
                      </Link>
                    );
                  })}
              </div>
            );
          }
        })}
    </div>
  );
}
