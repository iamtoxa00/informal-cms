import styles from "./CardChart.module.scss";

import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  BarChart,
  Bar,
  PieChart,
  Pie,
  Legend,
  Sector,
} from "recharts";
import { useEffect, useRef, useState } from "react";

function CustomizedBarShape(props) {
  const { fill, x, y, width, height } = props;

  const getPath = (x, y, width, height) =>
    `M${x} ${y + height} ${x + width} ${y + height} ${x + width} ${y + 5}Q${
      x + width
    } ${y} ${x + width - 5} ${y}L${x + 5} ${y}Q${x} ${y} ${x} ${y + 5}Z`;

  return <path d={getPath(x, y, width, height)} stroke="none" fill={fill} />;
}

export default function CardChart(props) {
  const { content, chart_type, ...otherProps } = props;

  const [activeIndex, setActiveIndex] = useState(null);
  const [show, setShow] = useState(false);

  useEffect(() => {
    setShow(true);
  }, []);

  const onPieEnter = (_, index) => {
    setActiveIndex(index);
  };

  const onPieLeave = () => {
    setActiveIndex(null);
  };

  const renderActiveShapePie = (props) => {
    const RADIAN = Math.PI / 180;
    const {
      cx,
      cy,
      midAngle,
      innerRadius,
      outerRadius,
      startAngle,
      endAngle,
      fill,
      payload,
      percent,
      value,
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? "start" : "end";

    return (
      <g>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius - 4}
          outerRadius={outerRadius + 4}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
      </g>
    );
  };

  return (
    <div className={styles.card_chart}>
      {show && (
        <div className={styles.chart_block}>
          {chart_type == "line" ? (
            <>
              <ResponsiveContainer width={"100%"} height={300}>
                <LineChart
                  margin={{ top: 24, right: 48, left: 0, bottom: 24 }}
                  data={props.data}
                >
                  <CartesianGrid stroke="#E0E0E0" />
                  <XAxis dataKey="name" tickCount={0} tickMargin={10} />
                  <YAxis tickMargin={10} />
                  <Tooltip cursor={{ stroke: "#AAA", strokeWidth: 2 }} />

                  {Object.keys(props.data[0])
                    .filter((el) => el != "name" && el != "color")
                    .map((key, index) => (
                      <Line
                        key={index}
                        type="monotone"
                        dataKey={key}
                        stroke={
                          (props.colors && props.colors[key]) || "#2D9CDB"
                        }
                        dot={{
                          fill:
                            (props.colors && props.colors[key]) || "#2D9CDB",
                          stroke:
                            (props.colors && props.colors[key]) || "#2D9CDB",
                          strokeWidth: 2,
                        }}
                        activeDot={{
                          fill:
                            (props.colors && props.colors[key]) || "#2D9CDB",
                          stroke: "#FFFFFF",
                          strokeWidth: 2,
                          r: 4,
                        }}
                      />
                    ))}
                </LineChart>
              </ResponsiveContainer>
            </>
          ) : chart_type == "pie" ? (
            <>
              <ResponsiveContainer width={"100%"} height={300}>
                <PieChart margin={{ top: 0, right: 0, left: 0, bottom: 32 }}>
                  <Pie
                    data={props.data}
                    dataKey="value"
                    nameKey="name"
                    cx="50%"
                    cy="50%"
                    innerRadius={"65%"}
                    outerRadius={"95%"}
                    fill="#E97979"
                    paddingAngle={1}
                    activeIndex={activeIndex}
                    activeShape={renderActiveShapePie}
                    onMouseEnter={onPieEnter}
                    onMouseLeave={onPieLeave}
                  />
                  <Tooltip cursor={{ stroke: "#AAA", strokeWidth: 2 }} />
                  <Legend
                    layout="vertical"
                    align="right"
                    verticalAlign="middle"
                    iconType="rect"
                  />
                </PieChart>
              </ResponsiveContainer>
            </>
          ) : chart_type == "bars" ? (
            <>
              <ResponsiveContainer width={"100%"} height={300}>
                <BarChart data={props.data}>
                  <CartesianGrid stroke="#E0E0E0" />
                  <XAxis dataKey="name" tickMargin={10} />
                  <YAxis tickMargin={10} />
                  <Tooltip
                    cursor={{ stroke: "#AAA", fill: "#AAA2", strokeWidth: 2 }}
                  />
                  <Bar
                    dataKey="pv"
                    fill="#E97979"
                    shape={<CustomizedBarShape />}
                  />
                  <Bar
                    dataKey="uv"
                    fill="#2D9CDB"
                    shape={<CustomizedBarShape />}
                  />
                </BarChart>
              </ResponsiveContainer>
            </>
          ) : (
            <></>
          )}
        </div>
      )}
    </div>
  );
}
