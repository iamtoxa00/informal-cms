import styles from './CardText.module.scss';
import ReactMarkdown from 'react-markdown'

export default function CardText(props) {
  const {content, ...otherProps} = props;

  return <div className={styles.wrapper}>
    {content.map((textBlock, index)=>{
      const TitleTag = (textBlock.title && textBlock.title.as) || 'h2';
      
      if(textBlock.type == 'paragraph'){
        return <div key={index} className={styles.text_block}>
          {textBlock.title && <TitleTag className={styles.text_block__title}>{textBlock.title.value || textBlock.title}</TitleTag>}
          <p className={styles.text_block__text}>{textBlock.text}</p>
        </div>
      }

      if(textBlock.type == 'markdown'){
        return <div key={index} className={styles.text_block}>
          <ReactMarkdown className={styles.markdown}>{textBlock.content}</ReactMarkdown>
        </div>
      }
    })}
  </div>;
}
