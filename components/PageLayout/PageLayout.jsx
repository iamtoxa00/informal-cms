import NavBar from 'components/NavBar/NavBar'
import PageHeader from 'components/PageHeader/PageHeader'
import styles from './PageLayout.module.scss'

export default function Cursor(props) {
    return (
        <>
            <NavBar/>

            <main className={styles.main}>
                <PageHeader/>
                <div className={styles.content} key={props.children.props.slug.join('_')}>
                    {props.children}
                </div>
            </main>
        </>
    )

}