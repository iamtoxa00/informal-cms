const path = require("path");
const withCSS = require('@zeit/next-css')
const withSass = require('@zeit/next-sass')

const withTM = require('next-transpile-modules')(['./components/AdminBlocks/TextBlock/TextBlock']);


module.exports = {
  ...withCSS(withSass()),
  future: {
    webpack5: false,
  },
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.resolve = {
      ...config.resolve,
      alias: {
        ...config.resolve.alias,
        "components": path.resolve(__dirname, "components/"),
        "styles": path.resolve(__dirname, "styles/"),
        "mocks": path.resolve(__dirname, "mocks/"),
        "utils": path.resolve(__dirname, "utils/"),
      },
    };

    // Important: return the modified config
    return config;
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/home',
        permanent: true,
      },
    ]
  }
};