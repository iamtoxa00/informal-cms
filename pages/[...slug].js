import Head from 'next/head'

import React from 'react';
import DynamicBlock from 'components/DynamicBlock/DynamicBlock';

export default function Home(props) {
  return (
    <>
      <Head>
        <title>Панель управления</title>
        <link rel="icon" type="image/svg+xml" href="/favicon.svg"/>
      </Head>

      {props.page.blocks && props.page.blocks.map((block, index)=>{
        return <DynamicBlock {...block} key={index}/>
      })}
    </>
  )
}

export async function getServerSideProps(context) {
  try{
    var pageData = require(`mocks/pages/${context.query.slug.join('/')}`).default;
  } catch (e){
    var pageData = {
      slug: '404',
      page: []
    };
  }
  return {
    props: {
      slug: context.query.slug,
      page: pageData
    },
  }
}