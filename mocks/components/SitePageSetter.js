export default {
  sections: [
    {
      name: 'Первый экран',
      data: [
        {
          label: 'Заголовок',
          name: 'title',
          value: 'Заголовок',
          type: 'input',
          input_type: 'text'
        },
        {
          label: 'Подзаголовок',
          name: 'subtitle',
          value: '',
          type: 'input',
          input_type: 'text'
        },
        {
          type: "group",
          content: [
            {
              label: 'Отправить',
              type: 'submit',
              columns: 6,
            },
            {
              type: "reset",
              label: "Отменить",
              columns: 6,
            },
          ],
        },
      ]
    },
    {
      name: 'О компании',
    },
    {
      name: 'Сотрудники',
    },
    {
      name: 'Прайслист',
    },
    {
      name: 'Подвал страницы',
    },
  ]
}