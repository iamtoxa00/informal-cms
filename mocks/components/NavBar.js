export default {
  width: 50,
  width_opened: 300,
  itemsGroups: [
    {
      items: [
        {
          label: 'Главный экран',
          url: '/home',
        }
      ]
    },
    {
      items: [
        {
          label: 'Страницы',
          url: '/pages',
          icon: 'Pages'
        }
      ]
    },
    {
      items: [
        {
          label: 'Хостинг',
          url: '/settings',
          icon: 'Cog'
        }
      ]
    }
  ]
}