export default {
  blocks: [
    {
      componentName: "CardBlock",
      columns: 12,
      theme: 'Warning',
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Тестовый режим",
            },
          ],
        },
        {
          content: [
            {
              text: "CMS работает на пробных данных, что ограничивает её функциональность",
              type: "paragraph",
            },
          ],
          type: "text",
        }
      ],
    },

  ],
};
