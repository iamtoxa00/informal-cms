export default {
  blocks: [
    {
      componentName: "CardBlock",
      columns: 12,
      theme: 'Warning',
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Тестовый режим",
            },
          ],
        },
        {
          content: [
            {
              text: "CMS работает на пробных данных, что ограничивает её функциональность",
              type: "paragraph",
            },
          ],
          type: "text",
        }
      ],
    },


    {
      componentName: "CardBlock",
      columns: 4,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Chart Example",
            },
          ],
        },
        {
          chart_type: 'line',
          type: "chart",
          data: [
            {
              name: "Page A",
              uv: 4000
            },
            {
              name: "Page B",
              uv: 3000
            },
            {
              name: "Page C",
              uv: 2000
            },
            {
              name: "Page D",
              uv: 2780
            },
            {
              name: "Page E",
              uv: 1890
            },
            {
              name: "Page F",
              uv: 2390
            },
            {
              name: "Page G",
              uv: 3490
            },
          ]
        }
      ],
    },

    {
      componentName: "CardBlock",
      columns: 4,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Chart Example",
            },
          ],
        },
        {
          chart_type: 'pie',
          type: "chart",
          data: [
            {
              name: "Жен.",
              value: 4000,
              fill: '#E97979'
            },
            {
              name: "Муж.",
              value: 3490,
              fill: '#2D9CDB'
            }
          ]
        },
      ],
    },

    {
      componentName: "CardBlock",
      columns: 4,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Chart Example",
            },
          ],
        },
        {
          chart_type: 'bars',
          type: "chart",
          data: [
            {
              name: "Page A",
              uv: 4000,
              pv: 2400
            },
            {
              name: "Page B",
              uv: 3000,
              pv: 1398
            },
            {
              name: "Page C",
              uv: 2000,
              pv: 9800
            },
            {
              name: "Page D",
              uv: 2780,
              pv: 3908
            },
            {
              name: "Page E",
              uv: 1890,
              pv: 4800
            },
            {
              name: "Page F",
              uv: 2390,
              pv: 3800
            },
            {
              name: "Page G",
              uv: 3490,
              pv: 4300
            },
          ]
        }
      ],
    },
  ],
};
