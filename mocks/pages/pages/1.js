export default {
  blocks: [
    {
      componentName: "CardBlock",
      columns: 12,
      theme: "Warning",
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Тестовый режим",
            },
          ],
        },
        {
          content: [
            {
              text:
                "CMS работает на пробных данных, что ограничивает её функциональность",
              type: "paragraph",
            },
          ],
          type: "text",
        },
      ],
    },

    {
      componentName: "CardBlock",
      columns: 3,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "SEO настройки",
            },
          ],
        },
        {
          type: "form",
          action_url: '/test',
          content: [
            {
              type: "input",
              input_type: "text",
              label: "Название страницы",
              name: "page_name",
              // value: 'Informal Place'
            },
            {
              type: "input",
              input_type: "text",
              label: "Краткое описание",
              name: "page_description",
              value: "A place that inspires",
            },
            {
              type: "input",
              input_type: "text",
              label: "URL страницы",
              name: "site_domain",
              value: "informalplace.ru/1",
              readOnly: true,
            },

            {
              type: "group",
              content: [
                {
                  type: "input",
                  input_type: "text",
                  label: "Поле 1",
                  name: "custom_field_1",
                  columns: 6,
                },
                {
                  type: "input",
                  input_type: "text",
                  label: "Поле 2",
                  name: "custom_field_2",
                  columns: 6,
                },
              ],
            },

            {
              type: "group",
              content: [
                {
                  type: "submit",
                  label: "Отправить",
                  columns: 6,
                },
                {
                  type: "reset",
                  label: "Отменить",
                  columns: 6,
                },
              ],
            },
          ],
        },
      ],
    },

    {
      componentName: "CardBlock",
      columns: 6,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Управление содержимым",
            }
          ],
        },
        {
          type: "card_pageSetter",
          page_slug: '/1'
        },
      ],
    },

    {
      componentName: "CardBlock",
      columns: 3,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Секретная плашка",
            },
          ],
        },
      ],
    },
  ],
};
