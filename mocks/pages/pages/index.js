export default {
  blocks: [
    {
      componentName: "CardBlock",
      columns: 12,
      theme: 'Warning',
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Тестовый режим",
            },
          ],
        },
        {
          content: [
            {
              text: "CMS работает на пробных данных, что ограничивает её функциональность",
              type: "paragraph",
            },
          ],
          type: "text",
        }
      ],
    },


    {
      componentName: "List",
      columns: 8,
      content: [
        {
          type: "item",
          content: [
            {
              type: "title",
              content: "Главная страница",
              as: 'h2',
              styledAs: 'h3'
            },
            {
              type: "subtitle",
              content: "Основная посадочная страница проекта",
              as: 'span',
              styledAs: 'p1'
            },
            {
              type: "links",
              content: [
                {
                  iconPos: 'after',
                  label: 'Редактировать',
                  url: '/pages/1',
                  icon: "Edit"
                },
                {
                  iconPos: 'after',
                  label: 'Открыть',
                  url: '#',
                  icon: "External"
                },
                {
                  iconPos: 'after',
                  label: 'Ссылка',
                  action: `()=>{console.log(this);}`,
                  icon: "Link"
                }
              ],
            },
          ],
        },
        {
          type: "item",
          content: [
            {
              type: "title",
              content: "Контакты",
              as: 'h2',
              styledAs: 'h3'
            },
            {
              type: "subtitle",
              content: "Страница с контактами компании",
              as: 'span',
              styledAs: 'p1'
            },
            {
              type: "links",
              content: [
                {
                  iconPos: 'after',
                  label: 'Редактировать',
                  url: '/pages/1',
                  icon: "Edit"
                },
                {
                  iconPos: 'after',
                  label: 'Открыть',
                  url: '#',
                  icon: "External"
                },
                {
                  iconPos: 'after',
                  label: 'Ссылка',
                  action: `()=>{console.log(this);}`,
                  icon: "Link"
                }
              ],
            },
          ],
        }
      ],
    },

    {
      componentName: "CardBlock",
      columns: 4,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Показатели наполнения страниц",
            },
          ],
        },
        {
          content: [
            {
              type: "card_list",
              content : [
                {
                  type: 'text',
                  content: 'На главной странице заданы не все OG теги'
                }
              ]
            },
          ]
        }
      ],
    },

  ],
};
