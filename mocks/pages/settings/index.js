export default {
  blocks: [
    {
      componentName: "CardBlock",
      columns: 12,
      theme: "Warning",
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Тестовый режим",
            },
          ],
        },
        {
          content: [
            {
              text:
                "CMS работает на пробных данных, что ограничивает её функциональность",
              type: "paragraph",
            },
          ],
          type: "text",
        },
      ],
    },

    {
      componentName: "CardBlock",
      columns: 6,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Просмотры страниц",
            },
          ],
        },
        {
          chart_type: "line",
          type: "chart",
          colors: {
            "Просмотры": "#E97979",
            "Посетители": "#2D9CDB",
          },
          data: [...Array(14)].map((val, index) => {
            return {
              name: `${index + 10}.04`,
              "Просмотры": Math.round(Math.random() * 1000 + 500),
              "Посетители": Math.round(Math.random() * 300 + 100),
            };
          }),
        },
      ],
    },

    {
      componentName: "CardBlock",
      columns: 6,
      content: [
        {
          type: "card_label",
          content: [
            {
              type: "text",
              content: "Место на диске",
            },
          ],
        },
        {
          chart_type: "pie",
          type: "chart",
          data: [
            {
              name: "Свободно",
              value:  Math.round(Math.random() * 1000 + 5000),
              fill: "#AAAAAA",
            },
            {
              name: "Занято",
              value:  Math.round(Math.random() * 1000 + 500),
              fill: "#2D9CDB",
            },
          ],
        },
      ],
    },
  ],
};
